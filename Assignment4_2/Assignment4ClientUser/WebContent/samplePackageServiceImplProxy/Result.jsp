<%@page contentType="text/html;charset=UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<HTML>
<HEAD>
<TITLE>Result</TITLE>
</HEAD>
<BODY>
<H1>Result</H1>

<jsp:useBean id="samplePackageServiceImplProxyid" scope="session" class="services.PackageServiceImplProxy" />
<%
if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
samplePackageServiceImplProxyid.setEndpoint(request.getParameter("endpoint"));
%>

<%
String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = samplePackageServiceImplProxyid.getEndpoint();
if(getEndpoint2mtemp == null){
%>
<%=getEndpoint2mtemp %>
<%
}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        %>
        <%= tempResultreturnp3 %>
        <%
}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        samplePackageServiceImplProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        services.PackageServiceImpl getPackageServiceImpl10mtemp = samplePackageServiceImplProxyid.getPackageServiceImpl();
if(getPackageServiceImpl10mtemp == null){
%>
<%=getPackageServiceImpl10mtemp %>
<%
}else{
        if(getPackageServiceImpl10mtemp!= null){
        String tempreturnp11 = getPackageServiceImpl10mtemp.toString();
        %>
        <%=tempreturnp11%>
        <%
        }}
break;
case 13:
        gotMethod = true;
        String packID_1id=  request.getParameter("packID16");
        int packID_1idTemp  = Integer.parseInt(packID_1id);
        java.lang.String checkPackageStatus13mtemp = samplePackageServiceImplProxyid.checkPackageStatus(packID_1idTemp);
if(checkPackageStatus13mtemp == null){
%>
<%=checkPackageStatus13mtemp %>
<%
}else{
        String tempResultreturnp14 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(checkPackageStatus13mtemp));
        %>
        <%= tempResultreturnp14 %>
        <%
}
break;
case 18:
        gotMethod = true;
        String username_2id=  request.getParameter("username21");
            java.lang.String username_2idTemp = null;
        if(!username_2id.equals("")){
         username_2idTemp  = username_2id;
        }
        beans.PackageClass[] listAllClientPackages18mtemp = samplePackageServiceImplProxyid.listAllClientPackages(username_2idTemp);
if(listAllClientPackages18mtemp == null){
%>
<%=listAllClientPackages18mtemp %>
<%
}else{
        String tempreturnp19 = null;
        if(listAllClientPackages18mtemp != null){
        java.util.List listreturnp19= java.util.Arrays.asList(listAllClientPackages18mtemp);
        tempreturnp19 = listreturnp19.toString();
        }
        %>
        <%=tempreturnp19%>
        <%
}
break;
case 23:
        gotMethod = true;
        String name_3id=  request.getParameter("name26");
            java.lang.String name_3idTemp = null;
        if(!name_3id.equals("")){
         name_3idTemp  = name_3id;
        }
        beans.PackageClass[] searchPackages23mtemp = samplePackageServiceImplProxyid.searchPackages(name_3idTemp);
if(searchPackages23mtemp == null){
%>
<%=searchPackages23mtemp %>
<%
}else{
        String tempreturnp24 = null;
        if(searchPackages23mtemp != null){
        java.util.List listreturnp24= java.util.Arrays.asList(searchPackages23mtemp);
        tempreturnp24 = listreturnp24.toString();
        }
        %>
        <%=tempreturnp24%>
        <%
}
break;
}
} catch (Exception e) { 
%>
Exception: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) %>
Message: <%= org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) %>
<%
return;
}
if(!gotMethod){
%>
result: N/A
<%
}
%>
</BODY>
</HTML>