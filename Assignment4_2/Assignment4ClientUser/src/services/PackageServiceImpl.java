/**
 * PackageServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public interface PackageServiceImpl extends java.rmi.Remote {
    public java.lang.String checkPackageStatus(int packID) throws java.rmi.RemoteException;
    public beans.PackageClass[] listAllClientPackages(java.lang.String username) throws java.rmi.RemoteException;
    public beans.PackageClass[] searchPackages(java.lang.String name) throws java.rmi.RemoteException;
}
