/**
 * UserServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public interface UserServiceImpl extends java.rmi.Remote {
    public int register(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
    public int login(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException;
}
