package view;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import services.PackageServiceImplProxy;
import services.UserServiceImplProxy;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MainFrame() {
		UserServiceImplProxy userService = new UserServiceImplProxy();
		PackageServiceImplProxy packageService = new PackageServiceImplProxy();
		
		setTitle("Online Tracking System");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		
		/////////////////////////////USER PANEL/////////////////////////////////
		JPanel userPanel = new JPanel();
		contentPane.add(userPanel, "userPanel");
		
		JLabel lblUsername = new JLabel("Username");	
		JTextField loginUsername = new JTextField();
		loginUsername.setColumns(15);
		
		JLabel lblPassword = new JLabel("Password");
		JPasswordField loginPassword = new JPasswordField();
		loginPassword.setColumns(15);
		
		JButton btnLogin = new JButton("Login");
		
		JLabel lblLoginMessage = new JLabel("");
		
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String username = loginUsername.getText();
				String password = loginPassword.getText();
				
				int loginStatus = 3;
				try {
					loginStatus = userService.login(username, password);
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				switch (loginStatus) {
				case 0: 
					lblLoginMessage.setText("You are not allowed to login. You should use the other application");
					pack();
					break;
				case 1:
					((CardLayout)contentPane.getLayout()).show(contentPane, "packagePanel");
					lblLoginMessage.setText("");
					pack();
					break;
				case 2:
					lblLoginMessage.setText("Inexistent user. Try to register.");
					pack();
					break;
				case 3:
					lblLoginMessage.setText("Incorrect password! Try again");
					pack();
					break;
				}
				
				loginUsername.setText("");
				loginPassword.setText("");
			}
		});
		
		GroupLayout gl_userPanel = new GroupLayout(userPanel);
		
		gl_userPanel.setHorizontalGroup(gl_userPanel.createParallelGroup()
				.addGroup(gl_userPanel.createSequentialGroup()
					.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.LEADING)
							.addComponent(lblUsername)
							.addComponent(lblPassword)
							.addComponent(btnLogin))
					.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.LEADING)
							.addComponent(loginUsername)
							.addComponent(loginPassword)))
				.addComponent(lblLoginMessage)
				);
		
		gl_userPanel.setVerticalGroup(gl_userPanel.createSequentialGroup()
				.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(lblUsername)
						.addComponent(loginUsername))
				.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(lblPassword)
						.addComponent(loginPassword))
				.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(btnLogin))
				.addComponent(lblLoginMessage)
				);
		
		userPanel.setLayout(gl_userPanel);
		gl_userPanel.setAutoCreateGaps(true);
		gl_userPanel.setAutoCreateContainerGaps(true);
		pack();

		
		/////////////////////////////PACKAGE PANEL/////////////////////////////////
//		JPanel packagePanel = new JPanel();
//		contentPane.add(packagePanel, "packagePanel");
//		
//		JButton btnAddPackage = new JButton("Add a new package");
//		JButton btnRemovePackage = new JButton("Remove a package");
//		JButton btnRegisterPackage = new JButton("Register package for tracking");
//		JButton btnUpdateStatus = new JButton("Update a package status");
//		JLabel lblMessage = new JLabel("");
//		JLabel lblSender = new JLabel("Sender:");
//		JLabel lblReceiver = new JLabel("Receiver:");
//		JLabel lblName = new JLabel("Name:");
//		JLabel lblDescription = new JLabel("Description:");
//		JLabel lblSendCity = new JLabel("Sender city:");
//		JLabel lblDestCity = new JLabel("Destination city:");
//		JTextField txtSender = new JTextField();
//		txtSender.setColumns(15);
//		JTextField txtReceiver = new JTextField();
//		txtReceiver.setColumns(15);
//		JTextField txtName = new JTextField();
//		txtName.setColumns(15);
//		JTextField txtDescription = new JTextField();
//		txtDescription.setColumns(15);
//		JTextField txtSendCity = new JTextField();
//		txtSendCity.setColumns(15);
//		JTextField txtDestCity = new JTextField();
//		txtDestCity.setColumns(15);
//		JLabel lblPackageID = new JLabel("Package ID:");
//		JTextField txtPackageID = new JTextField();
//		txtPackageID.setColumns(15);
//		
//		btnAddPackage.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				
//				try {
//					if (packageService.addPackage(txtSender.getText(), txtReceiver.getText(), txtName.getText(),
//							txtDescription.getText(), txtSendCity.getText(), txtDestCity.getText())) {
//						lblMessage.setText("New package added!");
//					} else {
//						lblMessage.setText("Error when adding a new package!");
//					}
//					pack();
//				} catch (RemoteException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//			}
//		});
//		
//		btnRemovePackage.addActionListener(new ActionListener() {
//			public void actionPerformed(ActionEvent e) {
//				try {
//					if (packageService.removePackage(Integer.parseInt(txtPackageID.getText()))) {
//						lblMessage.setText("Package removed successfully!");
//					} else {
//						lblMessage.setText("Error when removing a package!");
//					}
//					pack();
//				} catch (RemoteException e1) {
//					// TODO Auto-generated catch block
//					e1.printStackTrace();
//				}
//			}
//		});
//		
//		GroupLayout gl_packagePanel = new GroupLayout(packagePanel);
//		gl_packagePanel.setHorizontalGroup(gl_packagePanel.createParallelGroup()
//				.addGroup(gl_packagePanel.createSequentialGroup()
//						.addGroup(gl_packagePanel.createParallelGroup()
//							.addComponent(btnAddPackage)
//							.addComponent(btnRemovePackage)
//							.addComponent(btnRegisterPackage)
//							.addComponent(btnUpdateStatus))
//						.addGroup(gl_packagePanel.createParallelGroup()
//								.addComponent(lblSender)
//								.addComponent(lblReceiver)
//								.addComponent(lblName)
//								.addComponent(lblDescription)
//								.addComponent(lblSendCity)
//								.addComponent(lblDestCity)
//								.addComponent(lblPackageID))
//						.addGroup(gl_packagePanel.createParallelGroup()
//								.addComponent(txtSender)
//								.addComponent(txtReceiver)
//								.addComponent(txtName)
//								.addComponent(txtDescription)
//								.addComponent(txtSendCity)
//								.addComponent(txtDestCity)
//								.addComponent(txtPackageID)))
//				.addComponent(lblMessage)
//				);
//		
//		gl_packagePanel.setHorizontalGroup(gl_packagePanel.createSequentialGroup()
//				.addGroup(gl_packagePanel.createParallelGroup()
//						.addGroup(gl_packagePanel.createSequentialGroup()
//							.addComponent(btnAddPackage)
//							.addComponent(btnRemovePackage)
//							.addComponent(btnRegisterPackage)
//							.addComponent(btnUpdateStatus))
//						.addGroup(gl_packagePanel.createSequentialGroup()
//								.addComponent(lblSender)
//								.addComponent(lblReceiver)
//								.addComponent(lblName)
//								.addComponent(lblDescription)
//								.addComponent(lblSendCity)
//								.addComponent(lblDestCity)
//								.addComponent(lblPackageID))
//						.addGroup(gl_packagePanel.createSequentialGroup()
//								.addComponent(txtSender)
//								.addComponent(txtReceiver)
//								.addComponent(txtName)
//								.addComponent(txtDescription)
//								.addComponent(txtSendCity)
//								.addComponent(txtDestCity)
//								.addComponent(txtPackageID)))
//				.addComponent(lblMessage)
//				);
//
//		packagePanel.setLayout(gl_packagePanel);
	}
	
}
