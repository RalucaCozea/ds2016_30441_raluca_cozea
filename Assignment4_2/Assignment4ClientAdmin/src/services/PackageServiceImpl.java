/**
 * PackageServiceImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public interface PackageServiceImpl extends java.rmi.Remote {
    public boolean registerPackageForTracking(beans.PackageClass pack) throws java.rmi.RemoteException;
    public boolean addPackage(java.lang.String sender, java.lang.String receiver, java.lang.String name, java.lang.String description, java.lang.String senderCity, java.lang.String destinationCity) throws java.rmi.RemoteException;
    public boolean updatePackageStatus(beans.PackageClass pack) throws java.rmi.RemoteException;
    public boolean removePackage(int packID) throws java.rmi.RemoteException;
}
