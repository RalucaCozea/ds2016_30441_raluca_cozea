package services;

public class UserServiceImplProxy implements services.UserServiceImpl {
  private String _endpoint = null;
  private services.UserServiceImpl userServiceImpl = null;
  
  public UserServiceImplProxy() {
    _initUserServiceImplProxy();
  }
  
  public UserServiceImplProxy(String endpoint) {
    _endpoint = endpoint;
    _initUserServiceImplProxy();
  }
  
  private void _initUserServiceImplProxy() {
    try {
      userServiceImpl = (new services.UserServiceImplServiceLocator()).getUserServiceImpl();
      if (userServiceImpl != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)userServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)userServiceImpl)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (userServiceImpl != null)
      ((javax.xml.rpc.Stub)userServiceImpl)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public services.UserServiceImpl getUserServiceImpl() {
    if (userServiceImpl == null)
      _initUserServiceImplProxy();
    return userServiceImpl;
  }
  
  public int login(java.lang.String username, java.lang.String password) throws java.rmi.RemoteException{
    if (userServiceImpl == null)
      _initUserServiceImplProxy();
    return userServiceImpl.login(username, password);
  }
  
  
}