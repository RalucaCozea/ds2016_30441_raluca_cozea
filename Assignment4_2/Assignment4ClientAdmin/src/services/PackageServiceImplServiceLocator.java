/**
 * PackageServiceImplServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package services;

public class PackageServiceImplServiceLocator extends org.apache.axis.client.Service implements services.PackageServiceImplService {

    public PackageServiceImplServiceLocator() {
    }


    public PackageServiceImplServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public PackageServiceImplServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for PackageServiceImpl
    private java.lang.String PackageServiceImpl_address = "http://localhost:8181/Assignment4/services/PackageServiceImpl";

    public java.lang.String getPackageServiceImplAddress() {
        return PackageServiceImpl_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String PackageServiceImplWSDDServiceName = "PackageServiceImpl";

    public java.lang.String getPackageServiceImplWSDDServiceName() {
        return PackageServiceImplWSDDServiceName;
    }

    public void setPackageServiceImplWSDDServiceName(java.lang.String name) {
        PackageServiceImplWSDDServiceName = name;
    }

    public services.PackageServiceImpl getPackageServiceImpl() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(PackageServiceImpl_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getPackageServiceImpl(endpoint);
    }

    public services.PackageServiceImpl getPackageServiceImpl(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            services.PackageServiceImplSoapBindingStub _stub = new services.PackageServiceImplSoapBindingStub(portAddress, this);
            _stub.setPortName(getPackageServiceImplWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setPackageServiceImplEndpointAddress(java.lang.String address) {
        PackageServiceImpl_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (services.PackageServiceImpl.class.isAssignableFrom(serviceEndpointInterface)) {
                services.PackageServiceImplSoapBindingStub _stub = new services.PackageServiceImplSoapBindingStub(new java.net.URL(PackageServiceImpl_address), this);
                _stub.setPortName(getPackageServiceImplWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("PackageServiceImpl".equals(inputPortName)) {
            return getPackageServiceImpl();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://services", "PackageServiceImplService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://services", "PackageServiceImpl"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("PackageServiceImpl".equals(portName)) {
            setPackageServiceImplEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
