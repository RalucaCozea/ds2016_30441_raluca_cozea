package beans;

import java.io.Serializable;

public class PackageClass implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String sender;
	private String receiver;
	private String name;
	private String description;
	private String senderCity;
	private String destinationCity;
	private Boolean tracking;
	
	public PackageClass() {}
	
	public PackageClass(String sender, String receiver, String name, String description, String senderCity,
			String destinationCity) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.name = name;
		this.description = description;
		this.senderCity = senderCity;
		this.destinationCity = destinationCity;
		this.tracking = false;
	}

	public int getID() {
		return id;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(String senderCity) {
		this.senderCity = senderCity;
	}

	public String getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}

	public Boolean getTracking() {
		return tracking;
	}

	public void setTracking(Boolean tracking) {
		this.tracking = tracking;
	}
	
}
