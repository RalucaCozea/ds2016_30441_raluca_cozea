package services;

import beans.PackageClass;

public interface PackageService {

	PackageClass[] listAllClientPackages(String username);
	
	PackageClass[] searchPackages(String name);
	
	String checkPackageStatus(int packID);

	boolean addPackage(String sender, String receiver, String name, 
			String description, String senderCity, String destinationCity);

	boolean removePackage(int packID);
	
	boolean registerPackageForTracking(PackageClass pack);

	boolean updatePackageStatus(PackageClass pack);
}
