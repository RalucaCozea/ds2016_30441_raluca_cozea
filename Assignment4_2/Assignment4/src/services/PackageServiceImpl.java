package services;

import java.util.Set;

import beans.PackageClass;
import dao.PackageDAO;

public class PackageServiceImpl implements PackageService {

	@Override
	public PackageClass[] listAllClientPackages(String username) {
		PackageDAO packageDAO = new PackageDAO();
		Set<PackageClass> packsSet = packageDAO.getClientPackages(username);
		PackageClass[] packs = new PackageClass[packsSet.size()];
		
		int i=0;
		for(PackageClass pack: packsSet) {
			packs[i] = pack;
			i++;
		}
		
		return packs;
	}

	@Override
	public PackageClass[] searchPackages(String name) {
		PackageDAO packageDAO = new PackageDAO();
		Set<PackageClass> packsSet = packageDAO.searchPackages(name);
		PackageClass[] packs = new PackageClass[packsSet.size()];
		
		int i=0;
		for(PackageClass pack: packsSet) {
			packs[i] = pack;
			i++;
		}
		
		return packs;
	}

	@Override
	public String checkPackageStatus(int packID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addPackage(String sender, String receiver, String name, 
			String description, String senderCity, String destinationCity) {
		PackageDAO packageDAO = new PackageDAO();
		PackageClass pack = new PackageClass(sender, receiver, name, description, senderCity, destinationCity);
		
		return packageDAO.addPackage(pack);
	}

	@Override
	public boolean removePackage(int packID) {
		PackageDAO packageDAO = new PackageDAO();
		return packageDAO.removePackage(packID);
	}

	@Override
	public boolean registerPackageForTracking(PackageClass pack) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean updatePackageStatus(PackageClass pack) {
		// TODO Auto-generated method stub
		return false;
	}

}
