package services;

import beans.User;
import dao.UserDAO;

public class UserServiceImpl implements UserService {

	/**
	 * function login
	 * @return  0 if the user is simple client
	 * 			1 if the user is administrator
	 * 			2 if the user doesn't exist
	 * 			3 if the password is wrong
	 */
	@Override
	public int login(String username, String password) {
		UserDAO userDAO = new UserDAO();
		User user = userDAO.getUser(username);
		
		if (user != null) {
			if (password.equals(user.getPassword())) {
				return user.isAdmin() ? 1 : 0;
			} else {
				return 3;
			}
		} else {
			return 2; 
		}
	}
	
	/**
	 * function register
	 * @return  0 in case of success
	 * 			1 if the user already exists
	 * 			2 error at registration
	 */
	@Override
	public int register(String username, String password) {
		UserDAO userDAO = new UserDAO();
		User user = userDAO.getUser(username);
		
		if (user == null) {
			return userDAO.register(username, password) ? 0 : 2;
		} else {
			return 1;
		}
	}
}
