package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import beans.PackageClass;

public class PackageDAO {
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/assignment4";
	static final String USER = "root";
	static final String PASS = "maybeM3!";

	private Connection conn;

	public PackageDAO() {
		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean addPackage(PackageClass p) {
		try {
			Statement stmt;
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT LAST('id') FROM packages";
			ResultSet rs = stmt.executeQuery(sql);
			
			int id;
			id = rs.getInt("id");
			id++;
			
			sql = "INSERT INTO packages VALUES ('" + id + "', '" 
													+ p.getSender() + "', '" 
													+ p.getReceiver() + "', '"
													+ p.getName() + "', '"
													+ p.getDescription() + "', '"
													+ p.getSenderCity() + "', '"
													+ p.getDestinationCity() + "', '"
													+ (p.getTracking() ? 1 : 0) + "')";

			return (stmt.executeUpdate(sql) != 0);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean removePackage(int id) {
		try {
			Statement stmt;
			stmt = conn.createStatement();
			String sql;
			sql = "DELETE FROM packages WHERE id = '" + id + "'";

			return (stmt.executeUpdate(sql) != 0);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public Set<PackageClass> getAllPackages() {	
		Set<PackageClass> packages = new HashSet<PackageClass>();
		
		try {
			Statement stmt;
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT * FROM packages";
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				PackageClass p = new PackageClass();
				
				// Retrieve by column name
				p.setID(rs.getInt("id"));
				p.setSender(rs.getString("sender"));
				p.setReceiver(rs.getString("receiver"));
				p.setName(rs.getString("name"));
				p.setDescription(rs.getString("description"));
				p.setSenderCity(rs.getString("sendercity"));
				p.setDestinationCity(rs.getString("destinationcity"));
				p.setTracking(rs.getBoolean("tracking"));

				packages.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return packages;
	}
	
	public Set<PackageClass> getClientPackages(String username) {
		Set<PackageClass> packages = new HashSet<PackageClass>();
		
		try {
			Statement stmt;
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT * FROM packages WHERE sender = '" + username + "' OR receiver = '" + username + "'";
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				PackageClass p = new PackageClass();
				
				// Retrieve by column name
				p.setID(rs.getInt("id"));
				p.setSender(rs.getString("sender"));
				p.setReceiver(rs.getString("receiver"));
				p.setName(rs.getString("name"));
				p.setDescription(rs.getString("description"));
				p.setSenderCity(rs.getString("sendercity"));
				p.setDestinationCity(rs.getString("destinationcity"));
				p.setTracking(rs.getBoolean("tracking"));

				packages.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return packages;
	}
	
	public Set<PackageClass> searchPackages(String name) {
		Set<PackageClass> packages = new HashSet<PackageClass>();
		
		try {
			Statement stmt;
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT * FROM packages WHERE name = '" + name + "'";
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				PackageClass p = new PackageClass();
				
				// Retrieve by column name
				p.setID(rs.getInt("id"));
				p.setSender(rs.getString("sender"));
				p.setReceiver(rs.getString("receiver"));
				p.setName(rs.getString("name"));
				p.setDescription(rs.getString("description"));
				p.setSenderCity(rs.getString("sendercity"));
				p.setDestinationCity(rs.getString("destinationcity"));
				p.setTracking(rs.getBoolean("tracking"));

				packages.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return packages;
	}
}
