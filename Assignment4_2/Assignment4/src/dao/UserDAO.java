package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import beans.User;

public class UserDAO {

	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/assignment4";
	static final String USER = "root";
	static final String PASS = "maybeM3!";

	private Connection conn;

	public UserDAO() {
		try {
			// Register JDBC driver
			Class.forName("com.mysql.jdbc.Driver");

			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public User getUser(String username) {
		User user = null;
		
		try {
			Statement stmt;
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT * FROM users WHERE username = '" + username + "'";
			ResultSet rs = stmt.executeQuery(sql);

			// Extract data from result set
			while (rs.next()) {
				// Retrieve by column name
				user = new User();
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				user.setAdmin(rs.getBoolean("isAdmin"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return user;
	}
	
	/**
	 * Function register adds a new user in DB
	 * @param username
	 * @param password
	 * @return
	 */
	public boolean register(String username, String password) {
		try {
			Statement stmt;
			stmt = conn.createStatement();
			String sql;
			sql = "INSERT INTO users VALUES ('" + username + "', '" + password + "', 0)";

			return (stmt.executeUpdate(sql) != 0);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
