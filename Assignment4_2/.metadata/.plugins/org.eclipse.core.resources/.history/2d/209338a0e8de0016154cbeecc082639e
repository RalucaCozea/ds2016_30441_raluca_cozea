package view;

import java.awt.CardLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import beans.PackageClass;
import services.UserServiceImplProxy;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String currentUser;
	private JTable packagesTable;
	
	public MainFrame() {
		UserServiceImplProxy userService = new UserServiceImplProxy();
		
		setTitle("Online Tracking System");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		
		/////////////////////////////USER PANEL/////////////////////////////////
		JPanel userPanel = new JPanel();
		contentPane.add(userPanel, "userPanel");
		
		JLabel lblUsername = new JLabel("Username");	
		JTextField loginUsername = new JTextField();
		loginUsername.setColumns(15);
		
		JLabel lblPassword = new JLabel("Password");
		JPasswordField loginPassword = new JPasswordField();
		loginPassword.setColumns(15);
		
		JButton btnLogin = new JButton("Login");
		JButton btnRegister = new JButton("Register");
		
		JLabel lblLoginMessage = new JLabel("");
		
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String username = loginUsername.getText();
				String password = loginPassword.getText();
				
				int loginStatus = 3;
				try {
					loginStatus = userService.login(username, password);
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				currentUser = username;
				switch (loginStatus) {
				case 0: 
					((CardLayout)contentPane.getLayout()).show(contentPane, "packagePanel");
					lblLoginMessage.setText("");
					break;
				case 1:
					lblLoginMessage.setText("You are an administrator. You should use the other application");
					pack();
					break;
				case 2:
					lblLoginMessage.setText("Inexistent user. Try to register.");
					pack();
					break;
				case 3:
					lblLoginMessage.setText("Incorrect password! Try again");
					pack();
					break;
				}
				
				loginUsername.setText("");
				loginPassword.setText("");
			}
		});

		btnRegister.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String username = loginUsername.getText();
				String password = loginPassword.getText();
				
				int registerStatus = 2;
				try {
					registerStatus = userService.login(username, password);
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				switch (registerStatus) {
				case 0: 
					lblLoginMessage.setText("Successfully created a new user!");
					pack();
					break;
				case 1:
					lblLoginMessage.setText("Username already exists!");
					pack();
					break;
				case 2:
					lblLoginMessage.setText("Registration error.");
					pack();
					break;
				}
				
				loginUsername.setText("");
				loginPassword.setText("");
			}
		});

		GroupLayout gl_userPanel = new GroupLayout(userPanel);
		
		gl_userPanel.setHorizontalGroup(gl_userPanel.createParallelGroup()
				.addGroup(gl_userPanel.createSequentialGroup()
					.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.LEADING)
							.addComponent(lblUsername)
							.addComponent(lblPassword)
							.addComponent(btnLogin))
					.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.LEADING)
							.addComponent(loginUsername)
							.addComponent(loginPassword)
							.addComponent(btnRegister)))
				.addComponent(lblLoginMessage)
				);
		gl_userPanel.linkSize(SwingConstants.HORIZONTAL, btnLogin, btnRegister);
		
		gl_userPanel.setVerticalGroup(gl_userPanel.createSequentialGroup()
				.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.BASELINE)
						.addComponent(lblUsername)
						.addComponent(loginUsername))
				.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(lblPassword)
						.addComponent(loginPassword))
				.addGroup(gl_userPanel.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(btnLogin)
						.addComponent(btnRegister))
				.addComponent(lblLoginMessage)
				);
		
		userPanel.setLayout(gl_userPanel);
		gl_userPanel.setAutoCreateGaps(true);
		gl_userPanel.setAutoCreateContainerGaps(true);
		

		
		/////////////////////////////PACKAGE PANEL/////////////////////////////////
		JPanel packagePanel = new JPanel();
		contentPane.add(packagePanel, "packagePanel");
		
		JLabel lblWelcome = new JLabel("Welcome " + currentUser + "!");
		JButton btnList = new JButton("List all my packages");
		JButton btnSearch = new JButton("Search for packages");
		JButton btnCheck = new JButton("Check package status");
		JTextField packageName = new JTextField();
		
		GroupLayout gl_packagePanel = new GroupLayout(packagePanel);
		gl_packagePanel.setHorizontalGroup(gl_packagePanel.createParallelGroup()
				.addComponent(lblWelcome)
				.addComponent(packageName)
				.addGroup(gl_packagePanel.createSequentialGroup()
						.addComponent(btnList)
						.addComponent(btnSearch)
						.addComponent(btnCheck))
				//.addComponent(packagesTable)
				);
		
		gl_packagePanel.setVerticalGroup(gl_packagePanel.createSequentialGroup()
				.addComponent(lblWelcome)
				.addComponent(packageName)
				.addGroup(gl_packagePanel.createParallelGroup()
						.addComponent(btnList)
						.addComponent(btnSearch)
						.addComponent(btnCheck))
				.addComponent(packagesTable)
				);
		
		pack();
		packagesTable = new JTable();
	}
	
	private void fillPackagesTable(PackageClass[] packages) {
		String col[] = {"ID", "Sender", "Receiver", "Name", "Description", "Sender City", "Destination City"};
		
		DefaultTableModel tm = new DefaultTableModel(col, 0);
		packagesTable.setModel(tm);
		
		for (int i = 0; i < packages.length; i++) {
			PackageClass pkg = packages[i];
			
			int id = pkg.getID();
			String sender = pkg.getSender();
			String receiver = pkg.getReceiver();
			String name = pkg.getName();
			String desc = pkg.getDescription();
			String senderCity = pkg.getSenderCity();
			String destinationCity = pkg.getDestinationCity();
			
			Object[] rowData = {id,sender,receiver,name,desc,senderCity,destinationCity};
			
			tm.addRow(rowData);
		}
	}
}
