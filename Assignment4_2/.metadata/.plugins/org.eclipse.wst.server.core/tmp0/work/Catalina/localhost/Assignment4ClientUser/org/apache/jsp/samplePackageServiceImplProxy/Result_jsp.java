/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/9.0.0.M15
 * Generated at: 2017-01-19 23:23:15 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.samplePackageServiceImplProxy;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Result_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent,
                 org.apache.jasper.runtime.JspSourceImports {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private static final java.util.Set<java.lang.String> _jspx_imports_packages;

  private static final java.util.Set<java.lang.String> _jspx_imports_classes;

  static {
    _jspx_imports_packages = new java.util.HashSet<>();
    _jspx_imports_packages.add("javax.servlet");
    _jspx_imports_packages.add("javax.servlet.http");
    _jspx_imports_packages.add("javax.servlet.jsp");
    _jspx_imports_classes = null;
  }

  private volatile javax.el.ExpressionFactory _el_expressionfactory;
  private volatile org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public java.util.Set<java.lang.String> getPackageImports() {
    return _jspx_imports_packages;
  }

  public java.util.Set<java.lang.String> getClassImports() {
    return _jspx_imports_classes;
  }

  public javax.el.ExpressionFactory _jsp_getExpressionFactory() {
    if (_el_expressionfactory == null) {
      synchronized (this) {
        if (_el_expressionfactory == null) {
          _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
        }
      }
    }
    return _el_expressionfactory;
  }

  public org.apache.tomcat.InstanceManager _jsp_getInstanceManager() {
    if (_jsp_instancemanager == null) {
      synchronized (this) {
        if (_jsp_instancemanager == null) {
          _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
        }
      }
    }
    return _jsp_instancemanager;
  }

  public void _jspInit() {
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
      throws java.io.IOException, javax.servlet.ServletException {

    final java.lang.String _jspx_method = request.getMethod();
    if (!"GET".equals(_jspx_method) && !"POST".equals(_jspx_method) && !"HEAD".equals(_jspx_method) && !javax.servlet.DispatcherType.ERROR.equals(request.getDispatcherType())) {
      response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "JSPs only permit GET POST or HEAD");
      return;
    }

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write('\n');
 request.setCharacterEncoding("UTF-8"); 
      out.write("\n");
      out.write("<HTML>\n");
      out.write("<HEAD>\n");
      out.write("<TITLE>Result</TITLE>\n");
      out.write("</HEAD>\n");
      out.write("<BODY>\n");
      out.write("<H1>Result</H1>\n");
      out.write("\n");
      services.PackageServiceImplProxy samplePackageServiceImplProxyid = null;
      synchronized (session) {
        samplePackageServiceImplProxyid = (services.PackageServiceImplProxy) _jspx_page_context.getAttribute("samplePackageServiceImplProxyid", javax.servlet.jsp.PageContext.SESSION_SCOPE);
        if (samplePackageServiceImplProxyid == null){
          samplePackageServiceImplProxyid = new services.PackageServiceImplProxy();
          _jspx_page_context.setAttribute("samplePackageServiceImplProxyid", samplePackageServiceImplProxyid, javax.servlet.jsp.PageContext.SESSION_SCOPE);
        }
      }
      out.write('\n');

if (request.getParameter("endpoint") != null && request.getParameter("endpoint").length() > 0)
samplePackageServiceImplProxyid.setEndpoint(request.getParameter("endpoint"));

      out.write('\n');
      out.write('\n');

String method = request.getParameter("method");
int methodID = 0;
if (method == null) methodID = -1;

if(methodID != -1) methodID = Integer.parseInt(method);
boolean gotMethod = false;

try {
switch (methodID){ 
case 2:
        gotMethod = true;
        java.lang.String getEndpoint2mtemp = samplePackageServiceImplProxyid.getEndpoint();
if(getEndpoint2mtemp == null){

      out.write('\n');
      out.print(getEndpoint2mtemp );
      out.write('\n');

}else{
        String tempResultreturnp3 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(getEndpoint2mtemp));
        
      out.write("\n");
      out.write("        ");
      out.print( tempResultreturnp3 );
      out.write("\n");
      out.write("        ");

}
break;
case 5:
        gotMethod = true;
        String endpoint_0id=  request.getParameter("endpoint8");
            java.lang.String endpoint_0idTemp = null;
        if(!endpoint_0id.equals("")){
         endpoint_0idTemp  = endpoint_0id;
        }
        samplePackageServiceImplProxyid.setEndpoint(endpoint_0idTemp);
break;
case 10:
        gotMethod = true;
        services.PackageServiceImpl getPackageServiceImpl10mtemp = samplePackageServiceImplProxyid.getPackageServiceImpl();
if(getPackageServiceImpl10mtemp == null){

      out.write('\n');
      out.print(getPackageServiceImpl10mtemp );
      out.write('\n');

}else{
        if(getPackageServiceImpl10mtemp!= null){
        String tempreturnp11 = getPackageServiceImpl10mtemp.toString();
        
      out.write("\n");
      out.write("        ");
      out.print(tempreturnp11);
      out.write("\n");
      out.write("        ");

        }}
break;
case 13:
        gotMethod = true;
        String packID_1id=  request.getParameter("packID16");
        int packID_1idTemp  = Integer.parseInt(packID_1id);
        java.lang.String checkPackageStatus13mtemp = samplePackageServiceImplProxyid.checkPackageStatus(packID_1idTemp);
if(checkPackageStatus13mtemp == null){

      out.write('\n');
      out.print(checkPackageStatus13mtemp );
      out.write('\n');

}else{
        String tempResultreturnp14 = org.eclipse.jst.ws.util.JspUtils.markup(String.valueOf(checkPackageStatus13mtemp));
        
      out.write("\n");
      out.write("        ");
      out.print( tempResultreturnp14 );
      out.write("\n");
      out.write("        ");

}
break;
case 18:
        gotMethod = true;
        String username_2id=  request.getParameter("username21");
            java.lang.String username_2idTemp = null;
        if(!username_2id.equals("")){
         username_2idTemp  = username_2id;
        }
        beans.PackageClass[] listAllClientPackages18mtemp = samplePackageServiceImplProxyid.listAllClientPackages(username_2idTemp);
if(listAllClientPackages18mtemp == null){

      out.write('\n');
      out.print(listAllClientPackages18mtemp );
      out.write('\n');

}else{
        String tempreturnp19 = null;
        if(listAllClientPackages18mtemp != null){
        java.util.List listreturnp19= java.util.Arrays.asList(listAllClientPackages18mtemp);
        tempreturnp19 = listreturnp19.toString();
        }
        
      out.write("\n");
      out.write("        ");
      out.print(tempreturnp19);
      out.write("\n");
      out.write("        ");

}
break;
case 23:
        gotMethod = true;
        String name_3id=  request.getParameter("name26");
            java.lang.String name_3idTemp = null;
        if(!name_3id.equals("")){
         name_3idTemp  = name_3id;
        }
        beans.PackageClass[] searchPackages23mtemp = samplePackageServiceImplProxyid.searchPackages(name_3idTemp);
if(searchPackages23mtemp == null){

      out.write('\n');
      out.print(searchPackages23mtemp );
      out.write('\n');

}else{
        String tempreturnp24 = null;
        if(searchPackages23mtemp != null){
        java.util.List listreturnp24= java.util.Arrays.asList(searchPackages23mtemp);
        tempreturnp24 = listreturnp24.toString();
        }
        
      out.write("\n");
      out.write("        ");
      out.print(tempreturnp24);
      out.write("\n");
      out.write("        ");

}
break;
}
} catch (Exception e) { 

      out.write("\n");
      out.write("Exception: ");
      out.print( org.eclipse.jst.ws.util.JspUtils.markup(e.toString()) );
      out.write("\n");
      out.write("Message: ");
      out.print( org.eclipse.jst.ws.util.JspUtils.markup(e.getMessage()) );
      out.write('\n');

return;
}
if(!gotMethod){

      out.write("\n");
      out.write("result: N/A\n");

}

      out.write("\n");
      out.write("</BODY>\n");
      out.write("</HTML>");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try {
            if (response.isCommitted()) {
              out.flush();
            } else {
              out.clearBuffer();
            }
          } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
