<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%
	String loginError = (String)session.getAttribute("loginError");
	session.removeAttribute("loginError");
	session.removeAttribute("currentUser");
%>

<!DOCTYPE html>
<html>
<body>
<p class="error">
		<% if (loginError != null) {
			out.println(loginError);
		} %>
	</p>
	<div id="wrapper">
		<h1>Login</h1>
		<form id="loginForm" method="post" action="LoginServlet">
			<label for="username">Login:</label> 
			<input type="text" id="username" name="username" required/><br><br>
			<label for="password">Password:</label> 
			<input type="password" id="password" name="password" required/>
			<br><br>
			<div id="buttons">
				<input type="submit" value="Login" class="button">
			</div>
		</form>
	</div>
</body>
</html>
