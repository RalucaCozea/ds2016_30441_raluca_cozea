<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ page import="entities.User"%>
<%@ page import="entities.Flight"%>
<%@ page import="entities.City"%>
<%@ page import="java.util.List"%>

<% User user = (User)session.getAttribute("currentUser");
	if (user == null) {
		System.out.println("User is null.");
		response.sendRedirect("../loginPage.jsp");
		return;
	} else if (user.getIsAdmin() == 0) {
		System.out.println("Is a regular user.");
		response.sendRedirect("userPage.jsp");
		return;
	}
		
	List<Flight> allFlights = (List<Flight>)session.getAttribute("allFlights");
	List<City> allCities = (List<City>)session.getAttribute("allCities");
	List<User> allUsers = (List<User>)session.getAttribute("allUsers");
%>

<!DOCTYPE html>
<html>
<head></head>

<body>
<h3>Flights administration</h3>
<a href="../loginPage.jsp">LOG OUT</a>

	<table border="1" id="adminFlights">
		<tr>
			<th>Flight number</th>
			<th>Airplane type</th>
			<th>Departure city</th>
			<th>Departure</th>
			<th>Arrival city</th>
			<th>Arrival</th>
			<th>Client</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		
		<% for (Flight flight : allFlights) { %>
		<tr>
		<form method="post" action="AdminServlet">
			<td><input type="text" name="flightNumber" value="<%=flight.getFlightNumber()%>"  /></td>
			<td><input type="text" name="planeType" value="<%=flight.getPlaneType() %>"  /></td>
			<td><input type="text" name="departureCity" value="<%=flight.getDepartureCity() %>" /></td>
			<td><input type="text" name="departure" value="<%=flight.getDeparture() %>" /></td>
			<td><input type="text" name="arrivalCity" value="<%=flight.getArrivalCity() %>"  /></td>
			<td><input type="text" name="arrival" value="<%=flight.getArrival() %>" /></td>
			<td><input type="text" name="client" value="<%=flight.getClient() %>" /></td>				
			<td>
				<input type="hidden" name="updateFlight" value="<%=flight.getFlightNumber() %>"  />
				<input type="submit" name="formType" value="Update"/>	
			</td>
			<td>
				<input type="hidden" name="deleteFlight" value="<%=flight.getFlightNumber() %>"  />
				<input type="submit" name="formType" value="Delete" />
			</td>
		</form>
		</tr>
		<% } %>
		
		<tr>
			<form method="post" action="AdminServlet">
				<td><input type="text" name="newFlightNumber" required></input></td>
				<td><input type="text" name="newPlaneType" required></input></td>
				<td>
					<select name="newDepartureCity" required>
						<% for (City city : allCities) { %>
							<option value="<%=city.getName() %>"><%=city.getName() %></option>
						<% } %>
					</select>
				</td>
				<td><input type="text" name="newDeparture" required></input></td>
				<td>
					<select name="newArrivalCity" required>
						<% for (City city : allCities) { %>
							<option value="<%=city.getName() %>"><%=city.getName() %></option>
						<% } %>
					</select>
				</td>
				<td><input type="text" name="newArrival" required></input></td>
				<td>
					<select name="newClient" required>
						<% for (User client: allUsers) { %>
							<option value="<%=client.getUsername() %>"><%=client.getUsername() %></option>
						<% } %>
					</select>
				</td>
				<td>
					<input type="submit" name="formType" value="Create" />
				</td>
				<td></td>
			</form>
		</tr>
	</table>

</body>
</html>