package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entities.Flight;

public class FlightDAO {

	private SessionFactory sessionFactory;
	
	public FlightDAO(SessionFactory factory) {
		this.sessionFactory = factory;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> getUserFlights(String client) {
		List<Flight> flights = null;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE client=:client");
			query.setParameter("client", client);
			flights = (List<Flight>)query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				System.out.println(e);
			}
		} finally {
			session.close();
		}
		
		return flights;
	}
	
	@SuppressWarnings("unchecked")
	public List<Flight> getAllFlights() {
		List<Flight> flights = null;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight");
			flights = (List<Flight>)query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				System.out.println(e);
			}
		} finally {
			session.close();
		}
		
		return flights;
	}
	
	public boolean addFlight(Flight flight) {
		boolean success = false;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.save(flight);
			tx.commit();
			success = true;
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		
		return success;
	}
	
	public boolean updateFlight(Flight flight) {
		boolean success = false;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			session.update(flight);
			tx.commit();
			success = true;
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		
		return success;
	}
	
	public boolean deleteFlight(int flightNumber) {
		boolean success = false;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("Delete Flight WHERE flightNumber = :flightNumber");
			query.setParameter("flightNumber", flightNumber);
			query.executeUpdate();
			tx.commit();
			success = true;
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		
		return success;
	}
}
