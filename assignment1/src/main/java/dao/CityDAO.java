package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entities.City;

public class CityDAO {

	private SessionFactory sessionFactory;
	
	public CityDAO(SessionFactory factory) {
		this.sessionFactory = factory;
	}
	
	public City getCity(String name) {
		City city = null;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE city=:name");
			query.setParameter("name", name);
			city = (City)query.uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				System.out.println(e);
			}
		} finally {
			session.close();
		}
		
		return city;
	}
	
	@SuppressWarnings("unchecked")
	public List<City> getAllCities() {
		List<City> cities = null;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City");
			cities = (List<City>)query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				System.out.println(e);
			}
		} finally {
			session.close();
		}
		
		return cities;
	}
}
