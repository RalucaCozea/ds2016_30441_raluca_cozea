package dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entities.User;

public class UserDAO {

	private SessionFactory sessionFactory;
	
	public UserDAO(SessionFactory factory) {
		this.sessionFactory = factory;
	}
	
	public User getUser(String username) {
		User user = null;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE username=:username");
			query.setParameter("username", username);
			user = (User)query.uniqueResult();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				System.out.println(e);
			}
		} finally {
			session.close();
		}
		
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		List<User> users = null;
		Session session = sessionFactory.openSession();
		Transaction tx = null;
		
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE isAdmin=0");
			users = (List<User>)query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
				System.out.println(e);
			}
		} finally {
			session.close();
		}
		
		return users;
	}
}
