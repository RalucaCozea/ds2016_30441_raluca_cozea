package entities;

public class Flight {

	private int flightNumber;
	private String planeType;
	private String departureCity;
	private String departure;
	private String arrivalCity;
	private String arrival;
	private String client;
	
	public Flight() {
		super();
	}

	public Flight(int flightNumber, String planeType, String departureCity, String departure, String arrivalCity,
			String arrival, String client) {
		super();
		this.flightNumber = flightNumber;
		this.planeType = planeType;
		this.departureCity = departureCity;
		this.departure = departure;
		this.arrivalCity = arrivalCity;
		this.arrival = arrival;
		this.client = client;
	}

	public int getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(int flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getPlaneType() {
		return planeType;
	}

	public void setPlaneType(String planeType) {
		this.planeType = planeType;
	}

	public String getDepartureCity() {
		return departureCity;
	}

	public void setDepartureCity(String departureCity) {
		this.departureCity = departureCity;
	}

	public String getDeparture() {
		return departure;
	}

	public void setDeparture(String departure) {
		this.departure = departure;
	}

	public String getArrivalCity() {
		return arrivalCity;
	}

	public void setArrivalCity(String arrivalCity) {
		this.arrivalCity = arrivalCity;
	}

	public String getArrival() {
		return arrival;
	}

	public void setArrival(String arrival) {
		this.arrival = arrival;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}
	
}
