package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import dao.UserDAO;
import dao.FlightDAO;
import dao.CityDAO;

import entities.User;
import entities.Flight;
import entities.City;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		login(request, response);
	}
	
	private void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
		UserDAO userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());

		HttpSession session = request.getSession(false);
		
		User user = userDAO.getUser(username);
		
		if (user == null) {
			session.setAttribute("loginError", "User does not exist! Please try again.");
			response.sendRedirect("loginPage.jsp");
		}
		else if (!password.equals(user.getPassword())) {
			session.setAttribute("loginError", "Wrong password! Please try again.");
			response.sendRedirect("loginPage.jsp");
		} else {
			// Login succeeded
			session.setAttribute("currentUser", user);
			
			if (user.getIsAdmin() == 1) {
				List<Flight> allFlights = flightDAO.getAllFlights();
				session.setAttribute("allFlights", allFlights);
				
				List<City> allCities = cityDAO.getAllCities();
				session.setAttribute("allCities", allCities);
				
				List<User> allUsers = userDAO.getAllUsers();
				session.setAttribute("allUsers", allUsers);
				
				response.sendRedirect("pages/adminPage.jsp");
			} else {
				List<Flight> clientFlights = flightDAO.getUserFlights(username);
				session.setAttribute("clientFlights", clientFlights);

				List<City> allCities = cityDAO.getAllCities();
				session.setAttribute("allCities", allCities);
				
				response.sendRedirect("pages/userPage.jsp");
			}
		}
	}

}
