package servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.cfg.Configuration;

import dao.FlightDAO;
import entities.Flight;

/**
 * Servlet implementation class AdminServlet
 */
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String formType = request.getParameter("formType");
		
		if (formType.equals("Create")) {
			addNewFlight(request, response);
		} else if (formType.equals("Update")) {
			updateFlight(request, response);
		} else if (formType.equals("Delete")) {
			deleteFlight(request, response);
		}
		
		HttpSession session = request.getSession(false);		
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		List<Flight> allFlights = flightDAO.getAllFlights();
		
		session.setAttribute("allFlights", allFlights);
		
		response.sendRedirect("adminPage.jsp");
	}
	
	private void addNewFlight(HttpServletRequest request, HttpServletResponse response) {
		
		int flightNumber = Integer.parseInt(request.getParameter("newFlightNumber"));
		String planeType = request.getParameter("newPlaneType");
		String departureCity = request.getParameter("newDepartureCity");
		String departure = request.getParameter("newDeparture");
		String arrivalCity = request.getParameter("newArrivalCity");
		String arrival = request.getParameter("newArrival");
		String client = request.getParameter("newClient");
		
		Flight flight = new Flight(flightNumber, planeType, departureCity, departure, arrivalCity, arrival, client);
		
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		boolean responseDAO = flightDAO.addFlight(flight);
		
		System.out.println("New flight added: " + responseDAO);
	}

	private void updateFlight(HttpServletRequest request, HttpServletResponse response) {
		int flightNumber = Integer.parseInt(request.getParameter("flightNumber"));
		String planeType = request.getParameter("planeType");
		String departureCity = request.getParameter("departureCity");
		String departure = request.getParameter("departure");
		String arrivalCity = request.getParameter("arrivalCity");
		String arrival = request.getParameter("arrival");
		String client = request.getParameter("client");
		
		Flight flight = new Flight(flightNumber, planeType, departureCity, departure, arrivalCity, arrival, client);
		
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		boolean responseDAO = flightDAO.updateFlight(flight);
		
		System.out.println("Flight " + flightNumber + " updated: " + responseDAO);
	}

	private void deleteFlight(HttpServletRequest request, HttpServletResponse response) {
		int flightNumber = Integer.parseInt(request.getParameter("deleteFlight"));
		
		FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
		boolean responseDAO = flightDAO.deleteFlight(flightNumber);
		
		System.out.println("Flight " + flightNumber + " removed: " + responseDAO);
	}

}
