<%@page import="controller.DVDServices"%>
<%@page import="controller.DVDProducer"%>

<jsp:useBean id="obj" class="model.DVD"></jsp:useBean>

<jsp:setProperty property="*" name="obj"/>

<%
boolean status = DVDServices.validate(obj);
if(status) {
	DVDProducer.addDVD(obj);
	out.println("New DVD added!");
} else {
	out.println("Invalid data!");
}
%>

<jsp:include page="index.jsp"></jsp:include>