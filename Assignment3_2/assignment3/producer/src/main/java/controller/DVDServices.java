package controller;

import model.DVD;

public class DVDServices {

	public static boolean validate(DVD dvd) {
		return validateTitle(dvd.getTitle()) && validateYear(dvd.getYear()) && validatePrice(dvd.getPrice());
	}
	
	private static boolean validateTitle(String title) {
		return title != null;
	}
	
	private static boolean validateYear(int year) {
		return (year > 1900 && year < 2017);
	}
	
	private static boolean validatePrice(double price) {
		return price > 0;
	}
}
