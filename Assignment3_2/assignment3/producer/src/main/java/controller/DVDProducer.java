package controller;
import com.rabbitmq.client.ConnectionFactory;

import model.DVD;

import com.rabbitmq.client.Connection;

import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;

public class DVDProducer {

	private static final String EXCHANGE_NAME = "logs";

    public static void addDVD(DVD dvd)
                  throws java.io.IOException, TimeoutException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        String message = getMessage(dvd);
        TextFileService textFileService = new TextFileService();

        channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes());
        textFileService.createTextFile(message);
	    System.out.println(" [x] Sent '" + message + "'");
	    
	    channel.close();
	    connection.close();
	
	}

	private static String getMessage(DVD dvd) {
	    return dvd.getTitle() + " from year " + dvd.getYear() + " for only " + dvd.getPrice() + "!";
	}

}
