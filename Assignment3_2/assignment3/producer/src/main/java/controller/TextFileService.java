/**
 * 
 */
package controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author macbook
 *
 */
public class TextFileService {
	private static final String PATH_NAME = "/Users/macbook/Documents/DS2016_30441_Cozea_Raluca/Assignment3_2/assignment3/producer/Messages/";
	
	public void createTextFile(String text) throws IOException {
		Date date = new Date();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		
		File file = new File(PATH_NAME + "message_" + simpleDateFormat.format(date) + ".txt");
		//System.out.println("Path : " + file.getAbsolutePath());
		
		//Create the file
		if (file.createNewFile()){
			System.out.println("File is created!");
		} else {
			System.out.println("File already exists.");
		}
		
		FileWriter output = new FileWriter(file);
		output.write(text);
		output.close();
	}
}
