<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<%@ page import="entities.User"%>
<%@ page import="entities.Flight"%>
<%@ page import="entities.City"%>
<%@ page import="java.util.List"%>

<% User user = (User)session.getAttribute("currentUser");
	if (user == null) {
		System.out.println("User is null.");
		response.sendRedirect("../loginPage.jsp");
		return;
	} else if (user.getIsAdmin() == 1) {
		System.out.println("User is admin.");
		response.sendRedirect("adminPage.jsp");
		return;
	}

	List<Flight> clientFlights = (List<Flight>)session.getAttribute("clientFlights");
	session.removeAttribute("clientFlights");
	List<City> allCities = (List<City>)session.getAttribute("allCities");
	session.removeAttribute("allCities");
%>


<!DOCTYPE html>
<html>
<head></head>

<body>
<span><%=user.getUsername() %></span>
<a href="../loginPage.jsp">LOG OUT</a>
<h3>See your flights!</h3>

<table border="1" id="userFlights">
	<tr>
		<th>Flight number</th>
		<th>Airplane type</th>
		<th>Departure city</th>
		<th>Departure time</th>
		<th>Arrival city</th>
		<th>Arrival time</th>
	</tr>
	
	<% for(Flight flight: clientFlights) { %>
	<tr>
		<td><%= flight.getFlightNumber() %></td>
		<td><%= flight.getPlaneType() %></td>
		<td>
			<form method="get" action="UserServlet">
				<input type="submit" name="city" value="<%= flight.getDepartureCity() %>"/>
			</form>
		</td>
		<td><%= flight.getDeparture() %></td>
		<td>
			<form method="get" action="UserServlet">
				<input type="submit" name="city" value="<%= flight.getArrivalCity() %>"/>
			</form>
		</td>
		<td><%= flight.getArrival() %></td>
	</tr>
	<% } %>
</table>

<p>Click on a city to view local time</p>
<p>Local time: <span></span></p>
</body>
</html>