package services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import models.Car;

public class CarServices extends UnicastRemoteObject implements ICarServices {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CarServices() throws RemoteException {
		super();
	}

	@Override
	public double computeTax(Car car) throws RemoteException {
		int sum = 8;
		
		if (car.getEngineSize() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		
		if(car.getEngineSize() > 1601) { 
			sum = 18;
		} else if(car.getEngineSize() > 2001) {
			sum = 72;
		} else if(car.getEngineSize() > 2601) {
			sum = 144;
		} else if(car.getEngineSize() > 3001) {
			sum = 290;
		}
		
		return car.getEngineSize() / 200.0 * sum;
	}

	@Override
	public double computeSellPrice(Car car) throws RemoteException {
		return car.getPrice() - (car.getPrice() / 7) * (2015 - car.getYear());
	}

}
