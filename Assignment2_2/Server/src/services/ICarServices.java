package services;

import java.rmi.Remote;
import java.rmi.RemoteException;

import models.Car;

public interface ICarServices extends Remote {

	public double computeTax(Car car) throws RemoteException;
	
	public double computeSellPrice(Car car) throws RemoteException;
}
