package communication;



import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import services.CarServices;

public class ServerStart {

	public static void main(String[] args) {
		
		System.setProperty("java.security.policy","file:./security.policy");
		
		try {
			if (System.getSecurityManager() == null) {
				System.setSecurityManager(new SecurityManager());
			}
			
			Registry registry = LocateRegistry.createRegistry(9009);
			CarServices carServices = new CarServices();			   		   
			registry.bind("rmi://localhost/CarServices", carServices);
			System.out.println("Car Service Server is ready.");
		} catch (Exception e) {
			System.out.println("Car Service Server failed: " + e);
		}   
	}

}
