package controllers;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import communication.ClientCommunication;
import models.Car;

public class ClientController {

	ClientCommunication communication = new ClientCommunication();
	
	public double computeTax(int year, int engineSize, double price) throws AccessException, RemoteException, NotBoundException {
		Car car = new Car(year, engineSize, price);
		
		return communication.getCarServices().computeTax(car);
	}
	
	public double computeSellPrice(int year, int engineSize, double price) throws AccessException, RemoteException, NotBoundException {
		Car car = new Car(year, engineSize, price);
		
		return communication.getCarServices().computeSellPrice(car);
	}
}
