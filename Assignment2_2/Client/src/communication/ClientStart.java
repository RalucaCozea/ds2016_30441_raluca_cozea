package communication;

import java.awt.EventQueue;

import view.ClientView;

public class ClientStart {

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientView window = new ClientView();
					window.setFrame();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
