package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import controllers.ClientController;

public class ClientView {

	private JFrame frame;
	private ClientController controller;
	
	public ClientView() {
		controller = new ClientController();
		initialize();
	}
	
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 600, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		// declarations
		JTextField yearField = new JTextField();
		JLabel yearLabel = new JLabel("Year: ");
		JTextField engineSizeField = new JTextField();
		JLabel engineSizeLabel = new JLabel("Engine size: ");
		JTextField priceField = new JTextField();
		JLabel priceLabel = new JLabel("Price: ");
		JButton btnComputeTax = new JButton("Compute Tax");
		JButton btnComputePrice = new JButton("Compute Selling Price");
		JTextField resultField = new JTextField();
		JLabel resultLabel = new JLabel("Your result:");
		
		
		// setting bounds
		yearLabel.setBounds(30, 30, 80, 20);
		engineSizeLabel.setBounds(30, 60, 80, 20);
		priceLabel.setBounds(30, 90, 80, 20);
		yearField.setBounds(120, 30, 200, 20);
		engineSizeField.setBounds(120, 60, 200, 20);
		priceField.setBounds(120, 90, 200, 20);
		btnComputeTax.setBounds(370, 50, 200, 20);
		btnComputePrice.setBounds(370, 75, 200, 20);
		resultLabel.setBounds(150, 200, 100, 20);
		resultField.setBounds(280, 200, 200, 20);
		
		
		// adding action listeners
		btnComputeTax.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!yearField.getText().isEmpty() && !engineSizeField.getText().isEmpty() && !priceField.getText().isEmpty()) {
					
					int year = Integer.parseInt(yearField.getText());
					int engineSize = Integer.parseInt(engineSizeField.getText());
					double price = Double.parseDouble(priceField.getText());
					
					try {
						double result = controller.computeTax(year, engineSize, price);

						resultField.setText("" + result);
						resultLabel.setText("Sell price: ");
					} catch (AccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (NotBoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
			}
		});

		btnComputePrice.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (!yearField.getText().isEmpty() && !engineSizeField.getText().isEmpty() && !priceField.getText().isEmpty()) {
					
					int year = Integer.parseInt(yearField.getText());
					int engineSize = Integer.parseInt(engineSizeField.getText());
					int price = Integer.parseInt(priceField.getText());
					
					try {
						double result = controller.computeSellPrice(year, engineSize, price);
						
						resultField.setText("" + result);
						resultLabel.setText("Sell price: ");
					} catch (AccessException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (RemoteException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (NotBoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				
			}
		});
		
		// adding to content pane
		frame.getContentPane().add(yearLabel);
		frame.getContentPane().add(engineSizeLabel);
		frame.getContentPane().add(priceLabel);
		frame.getContentPane().add(yearField);
		frame.getContentPane().add(engineSizeField);
		frame.getContentPane().add(priceField);
		frame.getContentPane().add(btnComputeTax);
		frame.getContentPane().add(btnComputePrice);
		frame.getContentPane().add(resultLabel);
		frame.getContentPane().add(resultField);
	}
	
	public void setFrame() {
		this.frame.setVisible(true);
	}
}
